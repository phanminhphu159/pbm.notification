/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React,{ memo } from "react";
import {
	View,
	TouchableOpacity,
	StyleSheet,
	Animated,
	Dimensions,
} from "react-native";

const TabBar = ({ navigationState, position, setIndex }) => {
	
	console.log("TabBar render")
	
	const inputRange = navigationState.routes.map((x, i) => i);
	return (
		<View style={styles.container}>
			{navigationState.routes.map((route, index) => {
				const opacity = position.interpolate({
					inputRange,
					outputRange: inputRange.map((inputIndex) =>
						inputIndex === index ? 1 : 0.5
					),
				});
				return (
					<TouchableOpacity
						key={index}
						style={styles.tab}
						onPress={() => setIndex(index)}
					>
						<Animated.Text style={{ opacity, fontSize: 18 }}>
							{route.title}
						</Animated.Text>
					</TouchableOpacity>
				);
			})}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		borderColor: '#ccc',
		borderLeftWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderWidth: 1,
		flexDirection: 'row',
		height: 50,
		justifyContent: 'space-around'
	},
	tab: {
		alignItems: 'center',
		flex: 1,
		justifyContent: 'center',
		paddingBottom: 5
	}
});

export default memo(TabBar);
