/* eslint-disable react-native/no-inline-styles */
/* eslint-disable dot-notation */
/* eslint-disable react/display-name */
import React, { useState, memo } from 'react'
import {useWindowDimensions,StyleSheet } from 'react-native';
import { TabView, } from 'react-native-tab-view';

import categories from '../../services/emoji data/categories';
import EmojiCategory from './EmojiCategory';
import TabBar from './TabBar';

const EmojiPicker = ({onPress}) => {
	const layout = useWindowDimensions();
	const [index, setIndex] = useState(0);
	const [routes, setRoutes] = useState(categories.tabs.map(tab => ({ key: tab.category, title: tab.tabLabel })));
	console.log("emojipicker render")

	const renderScene = ( {route} ) => (
		<EmojiCategory
			category={route.key}
			onPress={onPress}
		/>
		
	)
	 
	return (
		<TabView
			renderTabBar={props => <TabBar setIndex={setIndex} {...props} />}
			navigationState={{index, routes}}
			onIndexChange={setIndex}
			renderScene={renderScene}
			initialLayout={{ width: layout.width }}
		/>
	)
}

export default memo(EmojiPicker);