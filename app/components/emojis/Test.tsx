/* eslint-disable react-native/no-inline-styles */
/* eslint-disable dot-notation */
/* eslint-disable react/display-name */
import React, { useState, memo } from 'react'
import {useWindowDimensions,FlatList, TouchableOpacity,Text,StyleSheet , View,ViewStyle} from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import categories from '../../services/emoji data/categories';
import { emojisByCategory } from '../../services/emoji data/emojis';
import shortnameToUnicode from '../../services/helpers/shortnameToUnicode';

import EmojiCategory from './EmojiCategory';
import TabBar from './TabBar';


const Screen = ({category, onPress}) => {
	console.log("category render")

	return (
		<FlatList
		data={emojisByCategory["frequentlyUsed"]}
		renderItem={({ item }) => 
			<TouchableOpacity style={styles.emojiContainer} onPress={() => {onPress(item)}}>
				<Text style={styles.emoji}>{shortnameToUnicode[`:${item}:`]}</Text>
			</TouchableOpacity>
		}
		keyExtractor={(item) => item}
		numColumns={8}
	/>
	);
}

const RenderScreen = memo(Screen);	

const sceneContainerStyle :ViewStyle = {
	// width: deviceWidth * this.props.children.length,
	flex: 0,
	flexDirection: 'row',
	// paddingBottom: 500
  };


const EmojiPicker = ({onPress}) => {
	const layout = useWindowDimensions();
	const [index, setIndex] = useState(0);
	const [routes, setRoutes] = useState(categories.tabs.map(tab => ({ key: tab.category, title: tab.tabLabel })));
	console.log("emojipicker render")

	const renderScene = ( {route} ) => (
		// <RenderScreen category={route.key} onPress={onPress}/>
		// <EmojiCategory
		// 	category={route.key}
		// 	onPress={onPress}
		// />
		
	)
	 
	return (
		<TabView
			renderTabBar={props => <TabBar setIndex={setIndex} {...props} />}
			navigationState={{index, routes}}
			onIndexChange={setIndex}
			renderScene={renderScene}
			initialLayout={{ width: layout.width }}
		/>
	)
}

const styles = StyleSheet.create({
	emoji: {
		fontSize: 20
	},
	emojiContainer: {
		marginHorizontal: 10,
		marginVertical: 4
	}
})


export default memo(EmojiPicker);