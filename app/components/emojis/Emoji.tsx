import React, { memo } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

import shortnameToUnicode from '../../services/helpers/shortnameToUnicode';

const Emoji = ({ item, onPress }) => {
	// console.log(item)
	return (
		<TouchableOpacity style={styles.emojiContainer} 
			onPress={() => {onPress(item)}}
		>
			<Text style={styles.emoji}>{shortnameToUnicode[`:${item}:`]}</Text>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	emoji: {
		fontSize: 20
	},
	emojiContainer: {
		marginHorizontal: 10,
		marginVertical: 4
	}
})

export default memo(Emoji)