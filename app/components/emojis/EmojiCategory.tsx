import React, { memo, useState } from 'react'
import { FlatList, Dimensions } from 'react-native'

import Emoji from './Emoji';
import { emojisByCategory } from '../../services/emoji data/emojis';


const EmojiCategory = ({ category, onPress}) => {
	console.log("emoji category render")
	
	return (
		<FlatList
			data={emojisByCategory[category]}
			renderItem={({ item }) => <Emoji item={item}  onPress={onPress} />}
			keyExtractor={(item) => item}
			numColumns={8}
		/>
	)
}


export default memo(EmojiCategory);