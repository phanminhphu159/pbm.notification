/* eslint-disable camelcase */
import React, { FC,useState, useEffect } from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity, Image, ImageStyle,TextInput,NativeModules, Button, Alert } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Text,
} from "../../components"
import { color, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import Fire from "../../services/firebase/Fire"
import FireStorage from "../../services/firebase/FireStorage"
import {StackActions  } from "@react-navigation/native"
import { v4 as uuidv4 } from 'uuid';


const FULL: ViewStyle = { flex: 1 }
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 20,
  lineHeight: 38,
  textAlign: "center",
  color: 'black'
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 10,
  paddingHorizontal: 10,
  alignItems: 'center',
  justifyContent: 'center',
}
const VIEW_HEADER_TEXT: ViewStyle = {
  marginLeft: 0
}
const IMAGE_GO_BACK_BTN: ImageStyle = {
  tintColor: "black", 
  height: 25,
  width: 25
}
const GO_BACK_STYLE: ViewStyle = {
  left: 15, 
  position: 'absolute'
}
const TEXTINPUT_STYLE: TextStyle = {
  color: "black",
  fontWeight: 'bold',
  fontSize: 14
}
const IMAGE: ImageStyle = {
  borderRadius: 40,
  height: 70,
  width: 70,
  marginRight: 30
}
const TEXT_INPUT: TextStyle = {
  marginLeft: 10,
  fontSize: 16,
  color: "black"
}

const INPUT: ViewStyle = { 
  flexDirection: 'row',
  marginHorizontal: 10,
  marginVertical: 30,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: "blue",
  alignItems: 'center',
  paddingHorizontal: 10
}
const INPUT1: ViewStyle = { 
  flexDirection: 'row',
  marginHorizontal: 10,
  marginVertical: 30,
  borderRadius: 10,
  alignItems: 'center',
  paddingHorizontal: 10
}

const VIEW_BUTTON: ViewStyle = { 
  marginTop: 100,
}
export const AddRoomScreen: FC<StackScreenProps<MessageNavigatorParamList, "add_room">> = observer(
  ({ navigation,route }) => {

    
    const ImagePicker = NativeModules.ImageCropPicker;


    const [Name,setName] = useState("")
    const [image_picker,setImagePicker] = useState(null)
    const [UserID_LOGGED_IN, setUser_LOGGED_IN] = useState(null);
    const [UserID_CHATWITH,setUserID_CHATWITH] = useState("")
    const temp = route.params;
    const FireBase = new Fire();
    const FireBaseStorage = new FireStorage();

    useEffect(() => {

      async function fetchData() {
        const userID = await FireBase.getUserID(temp);
        setUser_LOGGED_IN(userID);
      }
    
      fetchData()
    }, []);

    
    const goBack = () =>  { 
      navigation.dispatch(StackActions.replace('user_room',temp));
    };


    const pickImage=() => {
      ImagePicker.openPicker({
        multiple: false,
        waitAnimationEnd: true,
        includeExif: true,
        forceJpg: true,
      })
      .then(async img => {
        setImagePicker( img.path);
      }).catch(e => alert(e));
    }

    const createRoom = async () => {
      const roomID = "Room" + uuidv4();
      const productID = "Product" + uuidv4();
      const images_url_FireBase_Storage = await FireBaseStorage.handleUpload([{url: image_picker}],roomID);

      // Tạo room 
      FireBase.createRoom(images_url_FireBase_Storage,Name,roomID,productID)

      // Tạo product
      FireBase.createProduct(Name,productID)

      // Tạo participants
      FireBase.createParticipants(UserID_LOGGED_IN,roomID)
      FireBase.createParticipants(UserID_CHATWITH,roomID)

      Alert.alert("Tạo thành công");

    }


    return (
      <View testID="WelcomeScreen" style={FULL}>
      {/* // Header   Design  */}
        <View style={VIEW_HEADER}>
          <TouchableOpacity onPress={goBack} style={GO_BACK_STYLE}>
            <Image style={IMAGE_GO_BACK_BTN} resizeMode="cover" source={icons.left} />
          </TouchableOpacity>
          <View style={VIEW_HEADER_TEXT}>
            <Text style={TITLE}>Add product</Text>
          </View>
        </View>


        <View style={INPUT}>
          <Text style={TEXT_INPUT}>Tên room: </Text>
          <TextInput
                style={TEXTINPUT_STYLE}
                placeholder="..."
                onChangeText={Name => {
                  setName(Name);
                }}
                value={Name}
                placeholderTextColor={"grey"}
              >
          </TextInput>
        </View>
        
        <View style={INPUT1}>
          <Text style={TEXT_INPUT}>Ảnh mặt hàng:      </Text>
          { 
            image_picker !== null ? 
            <Image source={{ uri: image_picker }} style={IMAGE} /> 
            : null
          }
        <Button title="Chọn ảnh" onPress={pickImage}></Button>
        </View>
        
        <View style={INPUT}>
          <Text style={TEXT_INPUT}>chat với pbmX: </Text>
          <TextInput
                style={TEXTINPUT_STYLE}
                placeholder=" nhập userX_ID ( X -> 1-4 ) "
                onChangeText={UserID => {
                  setUserID_CHATWITH(UserID);
                }}
                value={UserID_CHATWITH}
                placeholderTextColor={"grey"}
              >
          </TextInput>
        </View>

        <View style={VIEW_BUTTON}>
          <Button title="Tạo room cho mặt hàng" onPress={createRoom}></Button>
        </View>
      </View>
    )
  },
)

