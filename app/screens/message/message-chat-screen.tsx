/* eslint-disable no-var */
/* eslint-disable camelcase */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { FC,useState, useEffect,useCallback } from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity, Image, SafeAreaView, LogBox, NativeModules,ActivityIndicator, Animated} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Text
} from "../../components"
import { color, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import { GiftedChat,Bubble } from "react-native-gifted-chat"
import Fire from "../../services/firebase/Fire"
import FireStorage from "../../services/firebase/FireStorage"
import Images from 'react-native-chat-images'
import { onChildAdded } from "firebase/database";
import EmojiPicker from "../../components/emojis/EmojiPicker";
// import ImagePicker from 'react-native-image-crop-picker'
// import isEqual from 'lodash.isequal';
// import {StackActions  } from "@react-navigation/native"  

const FULL: ViewStyle = { flex: 1 }
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
  color: 'black'
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 0,
  paddingHorizontal: 10,
  borderBottomWidth: 1,
  borderBottomColor: "black",
  alignItems: 'center',
  justifyContent: 'center',
}
const CONTAINER: ViewStyle = {
  justifyContent: "center",
  backgroundColor: "white",
  borderTopWidth: 1,
  borderTopColor: "grey"
}
const Footer: ViewStyle = { 
  flexDirection: 'row',
  marginVertical: 10,
  alignItems: 'center',
}

LogBox.ignoreLogs(["EventEmitter.removeListener"]);
LogBox.ignoreAllLogs();

export const MessageChatScreen: FC<StackScreenProps<MessageNavigatorParamList, "message_chat">> = observer(
  ({ navigation,route }) => {
    
    
    var ImagePicker = NativeModules.ImageCropPicker;

    function goBack  ()  { 
      navigation.navigate('user_room',userName);
    };
    const [Messages, setMessages] = useState(null);
    const [userName, setuserName] = useState(null);
    const [userAvatar, setuserAvatar] = useState(null);
    const [showEmojiPicker, setShowEmojiPicker] = useState(false);
    const [heightValue, setHeightValue] = useState(new Animated.Value(60));

    const route_params = route.params;
    const userID = route_params.userID;
    const roomID = route_params.roomID;
    const FireBase = new Fire();
    const FireBaseStorage = new FireStorage();
    
    // get Data && onChildAdded
    useEffect(() => {
      let Finishing = false;
      async function fetchData() {
        
        const messages = await new Promise((resolve,reject)=>{
          const messages = [];
          let dem = 1;
          let check = 1;
          onChildAdded(FireBase.dbMessages(roomID), async snapshot => {
              console.log(snapshot)
              const temp = dem++;
              const {userID, message, timestamp, image} = snapshot.val()
              const {key: id} = snapshot
              const createdAt = new Date(timestamp)
              
              const user = await FireBase.getUser(userID);

              const text = {
                  _id : id,
                  createdAt,
                  text: message,
                  image,
                  user: {
                      _id: userID,
                      name: user.name,
                      avatar: user.avatar
                  }
              }

              if (temp === check){
                  check++;
                  if (Finishing !== true){
                    messages.unshift(text);
                  }
                  resolve(messages); 
              }
              else{
                  setTimeout(() => {
                      if (temp === check){
                          check++;
                          if (Finishing !== true){
                            messages.unshift(text);
                          }
                          resolve(messages); 
                      }
                  }, 0);
              }
              if (Finishing === true){
                console.log("refresh messages")
                setMessages(previousMessages => GiftedChat.append(previousMessages, [text]))
              }
          });
        });
        
        Finishing = true;
        
        const temp = await FireBase.getUser(userID);
        setuserName(temp.name);
        setuserAvatar(temp.avatar);
        setMessages(messages);
    }
    
      fetchData()
    }, []);

    // emojis
    useEffect(() => {
      ShowEmojis();
      console.log("press");
    }, [showEmojiPicker])

    const ShowEmojis = () =>  { 
      Animated.timing(heightValue,{
        toValue: showEmojiPicker ? 250 : 60,
        duration: 0,
        useNativeDriver: false
      }).start();
    };
    

    // render Images
    const renderBubble =(props) => {
      const { currentMessage} = props;
      if ( Array.isArray(currentMessage.image ) ){
        return (
          <View style={{flex:1}}> 
            <Images style={{flex:1}} extra={"currentMessage.createdAt"} backgroundColor="#FFFFFF" images={currentMessage.image} />
          </View>
        );
      }
      return <Bubble {...props}/>
    }

    // send message
    const onSend = useCallback(( text ) => {
      const temp = text.map(item => {
        return ({
          message: item.text,
          userID: item.user._id,
        })
      });
      FireBase.send(temp,roomID)
    }, [])


    // Send img
    const pickMultiple=() => {
      ImagePicker.openPicker({
        multiple: true,
        waitAnimationEnd: true,
        includeExif: true,
        forceJpg: true,
      })
      .then(async img => {
        const images_url_local = img.map(i => {
          return {url: i.path, width: i.width, height: i.height, mime: i.mime};
        })
        const images_url_FireBase_Storage = await FireBaseStorage.handleUpload(images_url_local,roomID);
        onSendImage(images_url_FireBase_Storage,roomID);
      }).catch(e => alert(e));

    }
    
    const onSendImage = ( images,roomID ) => {
      const temp = {
        image: images,
        userID: userID,
      }
      FireBase.send(temp,roomID)
    }


    return (
      <View testID="WelcomeScreen" style={FULL}>
      {/* // Header Design  */}
      <View style={VIEW_HEADER}>
        <TouchableOpacity onPress={goBack} style={{left: 15, position: 'absolute'}}>
          <Image style={{tintColor: "black", height: 25, width: 25}} resizeMode="cover" source={icons.left} />
        </TouchableOpacity>
        <View style={{}}>
          <Text style={TITLE}>Chat Screen</Text>
        </View>
      </View>
      
      <SafeAreaView style={{flex:1, marginBottom: 0}}>
        <GiftedChat 
          // shouldUpdateMessage={(props, nextProps) => {
          //   if (!isEqual(currentRef,prevPref)) {
          //     return true;
          //   }
          // }}
          renderBubble = {renderBubble}
          showUserAvatar={true} 
          messages={Messages} 
          onSend={onSend} 
          renderLoading={() =>  <ActivityIndicator size="large" color="#0000ff" />}
          user={{_id: userID, name: userName, avatar: userAvatar }} 
          />
      </SafeAreaView>
      
      
      <Animated.View style={[{height: heightValue}, CONTAINER ]}>
        <View style={Footer}>
          <TouchableOpacity  
              style={{backgroundColor: "white", borderRadius: 15, padding: 5, marginLeft: 10}} 
              onPress={() => {setShowEmojiPicker(value => !value)}}
            >  
            <Image style={{width: 25, height: 25}} resizeMode="cover" source={icons.emoji} />
          </TouchableOpacity>

          <TouchableOpacity style={{marginLeft: 30}} onPress={pickMultiple}>
            <Image style={{height: 40, width: 40, marginLeft: 20}} source={icons.image}/>
          </TouchableOpacity>
        </View>
        <EmojiPicker onPress={val => console.log(val)}/> 
      </Animated.View>
      </View>

    )
  },
)

