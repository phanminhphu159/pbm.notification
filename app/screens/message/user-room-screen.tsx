import React, { FC,useState, useEffect } from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity, Image, FlatList,ImageStyle} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Text,
} from "../../components"
import { color, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import Fire from "../../services/firebase/Fire"
import {StackActions  } from "@react-navigation/native"


const FULL: ViewStyle = { flex: 1 }
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 20,
  lineHeight: 38,
  textAlign: "center",
  color: 'black'
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 10,
  paddingHorizontal: 10,
  alignItems: 'center',
  justifyContent: 'center',
}
const VIEW_HEADER_TEXT: ViewStyle = {
  marginLeft: 100
}
const LIST_CONTAINER: ViewStyle = {
  alignItems: "center",
  flexDirection: "row",
  padding: 10,
  paddingBottom: 15
}
const IMAGE: ImageStyle = {
  borderRadius: 40,
  height: 70,
  width: 70,
}
const IMAGE_ICON: ImageStyle = {
  width: 38,
  height: 38,
  tintColor:"black" ,
  alignSelf: 'center',
}
const LIST_TEXT: TextStyle = {
  marginLeft: 10,
  color: 'black',
  fontWeight: 'bold'
}
const EXTRA_TEXT: TextStyle = {
  marginLeft: 15, 
  marginVertical: 5, 
  color:'black'
}
const FLAT_LIST: ViewStyle = {
  paddingHorizontal: 10,
  paddingTop: 20
}
const IMAGE_GO_BACK_BTN: ImageStyle = {
  tintColor: "black", 
  height: 25,
  width: 25
}
const ADD_BTN_STYLE: ViewStyle = {
  marginLeft: 'auto',
  borderRadius: 50, 
  padding: 2, 
  backgroundColor: "#42DCD9"
}
const GO_BACK_STYLE: ViewStyle = {
  left: 15, 
  position: 'absolute'
}



export const UserRoomScreen: FC<StackScreenProps<MessageNavigatorParamList, "user_room">> = observer(
  ({ navigation,route }) => {

    
    const [RoomChat, setRoomChat] = useState(null);
    const [user, setuser] = useState(null);
    const Name = route.params;
    const FireBase = new Fire();

    useEffect(() => {
      async function fetchRoom() {
        // get userID
        let userID = await FireBase.getUserID(Name);
        if (userID != undefined || null || ""){ 
          // get all user's rooms
          let Room = await FireBase.getUserRoom(userID);
          let Allroom = await FireBase.getAllRoom(Room);
          // set data for the flat list
          setRoomChat(Allroom);
          setuser(userID);
        }   
      }
      fetchRoom()
    }, []);

    
    const next = (roomID) => {
      navigation.dispatch(StackActions.replace('message_chat',{roomID: roomID, userID: user}));
    }
    const Add = (item) => navigation.dispatch(StackActions.replace('add_room',item));
    const goBack = () =>  { 
      navigation.dispatch(StackActions.replace('message_home'));
    };

    return (
      <View testID="WelcomeScreen" style={FULL}>
      {/* // Header Design  */}
      <View style={VIEW_HEADER}>
        <TouchableOpacity onPress={goBack} style={GO_BACK_STYLE}>
          <Image style={IMAGE_GO_BACK_BTN} resizeMode="cover" source={icons.left} />
        </TouchableOpacity>
        <View style={VIEW_HEADER_TEXT}>
          <Text style={TITLE}>My Conversations</Text>
        </View>
        
        <TouchableOpacity  
              style={ADD_BTN_STYLE} 
              onPress={() => {Add(Name)}}
          >  
            <Image style={IMAGE_ICON} resizeMode="cover" source={icons.plus} />
         </TouchableOpacity>
      </View>

      
      <FlatList
        contentContainerStyle={FLAT_LIST}
        data={RoomChat}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <TouchableOpacity style={LIST_CONTAINER} onPress={() => next(item.id)} >
            <Image source={{ uri: item.image }} style={IMAGE} />
            <View>
              <Text style={LIST_TEXT}> {item.name}</Text>
              <Text style={EXTRA_TEXT}>Get latest message!!{item.text }</Text>
            </View>
          </TouchableOpacity>
        )}
      />
      </View>
    )
  },
)

