/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React, { FC, useState } from "react"
import { View, ViewStyle, TextStyle,TextInput,TouchableOpacity, Image,ImageStyle,StyleSheet} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Screen,
  Text,
  GradientBackground,
} from "../../components"
import { color, spacing, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import {StackActions  } from "@react-navigation/native"
// import EmojiSelector, { Categories } from "react-native-emoji-selector";
// import EmojiModal from 'react-native-emoji-modal';
import EmojiPicker from "../../components/emojis/EmojiPicker";




// const bowserLogo = require("./bowser.png")

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE_WRAPPER: TextStyle = {
  ...TEXT,
  textAlign: "center",
  marginTop: 15
}
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
}
const IMAGE_ICON: ImageStyle = {
  width: 18,
  height: 18,
  tintColor:"#FFFFFF" ,
  alignSelf: 'center',
}
const INPUT: ViewStyle = { 
  flexDirection: 'row',
  marginHorizontal: 10,
  marginVertical: 30,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: "#FFFFFF",
  alignItems: 'center',
  paddingHorizontal: 10
}
const styles = StyleSheet.create({
  input: {
    color: "#FFFFFF",
    fontSize: 18,
    fontWeight: 'bold',
  }
})

export const MessageScreen: FC<StackScreenProps<MessageNavigatorParamList, "message_home">> = observer(
  ({ navigation }) => {
    const [Name,setName] = useState("")
    const [isShowEmoji,setisShowEmoji] = useState(false)
    // const next = (item) => navigation.navigate("user_room",item)
    const next = (item) => navigation.dispatch(StackActions.replace('user_room',item));


    return (
      <View testID="WelcomeScreen" style={FULL}>
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} preset="scroll" backgroundColor={color.transparent}>
          <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text="Login Screen" />
          </Text>
          
            <View style={INPUT}>
            <TextInput
              style={styles.input}
              placeholder="..."
              onChangeText={Name => {
                setName(Name);
              }}
              value={Name}
              placeholderTextColor={"grey"}
            >
            </TextInput>
            <TouchableOpacity  
              style={{marginLeft: 'auto', backgroundColor: "#42DCD9", borderRadius: 15, padding: 8}} 
              onPress={() => {next(Name)}}
            >  
              <Image style={IMAGE_ICON} resizeMode="cover" source={icons.next} />
            </TouchableOpacity>
          </View>


          {/* <TouchableOpacity  
              style={{marginRight: 'auto', backgroundColor: "white", borderRadius: 15, padding: 8}} 
              onPress={() => {ShowEmoji()}}
            >  
            <Image style={{width: 25, height: 25}} resizeMode="cover" source={icons.emoji} />
          </TouchableOpacity> */}
          {/* { 
            isShowEmoji !== false ? 
            <EmojiModal 
            category={Categories.symbols}
            onEmojiSelected={(emoji) => {setName(Name + emoji)}} 
            /> 
            : null
          }
           */}
          

        </Screen>
			{/* <EmojiPicker /> */}
      </View>
    )
  },
)
  