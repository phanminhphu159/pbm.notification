import { initializeApp,getApp } from 'firebase/app';
import {getAuth,onAuthStateChanged,signInAnonymously} from "firebase/auth";
// import {push} from "firebase/database";
import {getStorage,ref,uploadBytes,getDownloadURL} from "firebase/storage";
// import { uuid } from 'uuidv4';
import { v4 as uuidv4 } from 'uuid';

class FireStorage {
    constructor() {
        // this.init()
        // this.checkAuth()
    }


    checkAuth = () => {
        const auth = getAuth();

        onAuthStateChanged(auth, user => {
            if (user != null) {
                signInAnonymously(auth);
            }
        });
    }

    handleUpload = async (images_url_local, roomID) => {
        console.log(images_url_local)
        const storage = getStorage();
        let url = [];

        for (const element of images_url_local) {
            
            const uuid = uuidv4();
            let reference = ref(storage,"images/"+ roomID +"/" + uuid);
            
            let img = await fetch(element.url);
            let bytes = await img.blob();
            let metadata = { contentType: 'image/jpeg', };
            
            await uploadBytes(reference, bytes,metadata);

            let temp = await getDownloadURL(reference);
            url.push(temp);
        }
        return url;
    };

    get uid(){
        const auth = getAuth();
        const user = auth.currentUser;
        return (user || {}).uid;
    }
}

export default FireStorage