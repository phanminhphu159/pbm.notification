/* eslint-disable camelcase */
import { initializeApp,getApp } from 'firebase/app';
import {getAuth,onAuthStateChanged,signInAnonymously} from "firebase/auth";
import { getDatabase, ref,serverTimestamp,push,onChildAdded,off } from "firebase/database";


class Fire {
    constructor() {
        this.init()
        this.checkAuth()
    }
    

    init = () => {
        if (!getApp.length) {
            initializeApp({
                apiKey: "AIzaSyD3Bk4hGzJqaa8SQ6REJWiWg8TKMVgHhi0",
                authDomain: "notification-69f68.firebaseapp.com",
                databaseURL: "https://notification-69f68-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "notification-69f68",
                storageBucket: "notification-69f68.appspot.com",
                messagingSenderId: "773700716215",
                appId: "1:773700716215:web:ba4be7608faefb21016f32",
                measurementId: "G-TCY37984HH"
            })
        }
    }


    checkAuth = () => {
        const auth = getAuth();
        onAuthStateChanged(auth, user => {
            if (user != null) {
                signInAnonymously(auth);
            }
        });
    }


    // Users
    getUserID = (userName) =>{
        return new Promise((resolve,reject)=>{
            console.log('fetching id');
            onChildAdded(this.db_User, (snapshot) => {
                if (snapshot.val().name === userName){
                    resolve(snapshot.val().userID);    
                }
            }); 
        });
    }

    getUser = (userID) =>{
        return new Promise((resolve,reject)=>{
            let temp = {};
            onChildAdded(this.db_User, async snapshot => {
                if (snapshot.val().userID == userID){
                    temp = await { name : snapshot.val().name, avatar : snapshot.val().avatar } 
                    resolve(temp);   
                }
            });
        });
    }

    // Room
    getRoom = async roomID => {
        return new Promise((resolve,reject)=>{
            let room = {};
            onChildAdded(this.db_Room, async snapshot => {
                if (snapshot.val().roomID == roomID){
                    room = await { id: snapshot.key, name : snapshot.val().name, image: snapshot.val().image, text:  snapshot.val().text  }
                    resolve(room);
                }
            });
        });
    }

    // dasdsa 
    // Participants
    
    getUserRoom = (userID)=>{
        console.log('get user Room');
        return new Promise((resolve,reject)=>{
            onChildAdded(this.db_Participants, snapshot => {
                if (snapshot.val().userID === userID){
                    // get room
                    resolve(snapshot.key);
                }
            });
        });
    }

    getAllRoom = (link)=>{
        return new Promise((resolve,reject)=>{
            let temp = [];
            onChildAdded(this.dbParticipants(link), async snapshot => {
                // get room
                let room = await this.getRoom(snapshot.val().roomID);
                temp.push(room); 
                resolve(temp);
            });
        });
    }



    // Messages   
    getMessages =   roomID => {
        return new Promise((resolve,reject)=>{
            let messages = [];
            let dem = 1;
            let check = 1;
            onChildAdded(this.dbMessages(roomID), async snapshot => {
                console.log(snapshot)
                let temp = dem++;
                const {userID, message, timestamp, image} = snapshot.val()
                const {key: id} = snapshot
                const createdAt = new Date(timestamp)
                
                let user = await this.getUser(userID);

                let text = {
                    _id : id,
                    createdAt,
                    text: message,
                    image,
                    user: {
                        _id: userID,
                        name: user.name,
                        avatar: user.avatar
                    }
                }

                if (temp === check){
                    check++;
                    messages.unshift(text);
                    resolve(messages); 
                }
                else{
                    setTimeout(() => {
                        if (temp === check){
                            check++;
                            messages.unshift(text);
                            resolve(messages); 
                        }
                    }, 0);
                }
            });
        });
    }




    send = (data, RoomID) => {
        let value = {}

        if (data.image === undefined || null){
            data.forEach(item => {
                value = {
                    message:  item.message,
                    timestamp: serverTimestamp(),
                    userID: item.userID
                }
            });
        }        
        else{
            value = {
                image: data.image.length > 1 ? data.image : data.image.toString(),
                timestamp: serverTimestamp(),
                userID: data.userID
            }
        }
        push(this.dbMessages(RoomID),value)
        
    }

    createRoom = (image,name, roomID,productID) => {
        const value = {
            image:image.length > 1 ? image : image.toString(),
            name: name,
            productID: productID,
            roomID:roomID,
            type: "kieu room"
        }

        push(this.db_Room,value)
    }

    
    createProduct = (name, productID) => {
        const value = {
            name: name,
            productID: productID,
            type: "type"
        }

        push(this.db_Products,value)
    }

    createParticipants = (userID,roomID) => {
        const value ={
            roomID: roomID,
        }

        // return new Promise((resolve,reject)=>{
            onChildAdded(this.db_Participants, snapshot => {
                if (snapshot.val().userID === userID){
                    // get room
                    push(this.dbParticipants(snapshot.key),value)
                    // resolve(null);
                }
            });
        // });
    }



    
    get db_User(){
        const db = getDatabase();
        return ref(db,"users");
    }
    
    get db_Room(){
        const db = getDatabase();
        return ref(db,"room");
    }

    get db_Participants(){
        const db = getDatabase();
        return ref(db,"participants");
    }

    get db_Products(){
        const db = getDatabase();
        return ref(db,"products");
    }

    dbParticipants(Participants){
        const db = getDatabase();
        return ref(db,"participants/"+ Participants + "/rooms");
    }   

    dbMessages(roomID){
        const db = getDatabase();
        return ref(db,"room/"+ roomID + "/messages");
    }

    off(){
        off(this.db_User);
        off(this.db_Room);
        off(this.db_Participants);
    }

}

export default Fire